import numpy as np
import scipy as sp

print('holas')

n = 500
p = 2


n = 9
p = 3

entrada         = []
salida          = []
learning_rate   = []

# clase de la capa de la red
class neural_layer():
    def __init__(self, n_conn, n_neur, act_f):
        self.act_f = act_f
        self.b = np.random.rand(1, n_neur)
        self.W = np.random.rand(n_conn, n_neur)
        
# funciones de activación    
sigm = (lambda x: 1 / (1 + np.e ** (-x)),
       lambda x: x * (1 - x))         

relu = lambda x: np.maximum(0, x)

def create_nn(topology, act_f):
    nn = []
    for i, layer in enumerate(topology[:-1]):
        nn.append(neural_layer(topology[l], topology[l+1], act_f))
        
    return nn

topology = [p, 4, 8, 16, 8, 4, 9]

neural_net = create_nn(topology, sigm)

l2_cost = (lambda Yp, Yr: np.mean((Yp - Yr) ** 2),
           lambda Yp, Yr: (Yp - Yr ))

def train(neural_net, entrada, salida, l2_cost, lr=learning_rate, train=True):

    # forward pass
    out = [(None, X)]
    for l, layer in enumerate(neural_net):
        z = out[-1][1] @ neural_net[l].W + neural_net[l].b # suma ponderada de la primer capa
        a = neural_net[l].act_f[0](z)
        out.append((z, a))

    if train:

        # backward pass 
        deltas = []

        for l in reversed(range(0, len(neural_net))):
            z = out[l+1][0]
            a = out[l+1][1]
            if l == len(neural_net) - 1:
                deltas.insert(0, l2_cost[1](a, Y) * neural_net[l].act_f[1](a))
            else:
                deltas.insert(0, deltas[0] @ _W.T * neural_net[l].act_f[1](a))

        _W = neural_net[l].W

        # gradient descent
        neural_net[1].b = neural_net[1].b - np.mean(deltas[0], axis=0, keepdims=True) * lr
        neural_net[1].W = neural_net[1].W - out[l][1].T @ deltas[0] * lr

    return out[-1][

train(neural_net, X, 
