import sys
import json
import math
import logging
from match import Match
from neuron import Neuron
from connection import Connection

logging.basicConfig(filename='log/debug.log',level=logging.DEBUG,format='[%(levelname)s] %(asctime)s %(message)s', datefmt='%Y/%m/%dT%H:%M:%S')

network_name    = sys.argv[1]
layers_sizes    = [9,9,24,9,9]

letters = ['a','b','c','d','e','f','g','h','i']

network_file    = open("networks/"+network_name+".json","r")
network_json    = network_file.read()
network_values  = json.loads(network_json)

def basicFunction(x):
    return x

def reduceNegatives(x):
    if (x < 0):
        return x * 0.98
    else:
        return x

def power3abs(x):
    result = math.fabs(x**3)
    return result

functions = {
    "basicFunction"     : basicFunction,
    "reduceNegatives"   : reduceNegatives,
    "power3abs"         : power3abs
}

def resetNet():
    network = []

    for layer_index in range(len(layers_sizes)):
        for neuron_index in range(layers_sizes[layer_index]):
            #neuron connections
            neuron_connections      = []
            if layer_index < len(layers_sizes)-1:
                target_neurons_offset   = 0
                previous_layer_index    = 0
                while previous_layer_index <= layer_index:
                    target_neurons_offset += layers_sizes[previous_layer_index]
                    previous_layer_index += 1
                for target_neuron_index in range(layers_sizes[layer_index+1]):
                    weight = network_values["connections"][layer_index][neuron_index][target_neuron_index]
                    neuron_connections.append( Connection(weight,target_neuron_index + target_neurons_offset) )
                    # logging.debug('connecting ' + str(neuron_index) + ' with ' + str(target_neuron_index + target_neurons_offset) + ' with weight: ' + str(weight))

            #neuron itself
            if layer_index == 0 or layer_index == len(layers_sizes) - 1:
                function    = basicFunction
                bias        = 0
            else:
                function    = functions[network_values["functions"][layer_index-1]] # -1 because in functions list contains only hidden layer, so indexes are shifted respect to the list of all layers
                bias        = network_values["bias"][layer_index-1][neuron_index]   # -1 because in bias list contains only hidden layer, so indexes are shifted respect to the list of all layers
            neuron = Neuron(function,bias,network,neuron_index)
            neuron.setConnections(neuron_connections)
            network.append(neuron)
    return network

def playMove(inputs,network):
    output      = []
    counter     = 0
    threshold   = len(network)-10
    next_offset = 0
    layer_index = 0
    local_count = 0
    print(inputs)
    for neuron_index in range(layers_sizes[0]):
        network[neuron_index].setBias(inputs[neuron_index])
    for neuron in network:
        res = neuron.activate()
        if counter == next_offset:
            logging.debug('---------- layer '+str(layer_index)+' ------------')
            layer_index += 1
            next_offset += layers_sizes[layer_index-1]
            local_count = 0
        logging.debug('neurona ' + str(local_count) + ', input ' + str(neuron.input_value) + ' result: ' + str(res) + ' function:')
        if counter > threshold:
            output.append(res)
        counter     += 1
        local_count += 1
    return output
'''
letras = ['a','b','c','d','e','f','g','h','i']
inputs = [1,1,0,0,-1,0,-1,-1,0]
result = playMove(inputs)
for i,v in enumerate(result):
    print(letras[i] + ': ' + str(v))
'''
#
# play match
#

def getInvertedBoard(board):
    inverted = []
    for i in board:
        inverted.append(-i)
    return inverted

def getPosFromOutput(board,outputs):
    map = [4, 0, 2, 6, 8, 1, 3, 5, 7]
    #recorro el list de outputs (orden santi)
    biggestIndex = 0
    biggestValue = 0
    for index, value in enumerate(outputs):
        if ( board[index] != 0):
            continue
        if value > biggestValue:
            biggestValue = value
            biggestIndex = index
    return map[biggestIndex]


matchBoard  = [0,0,0,0,0,0,0,0,0]
match       = Match('OX')

map = [4, 0, 2, 6, 8, 1, 3, 5, 7]
match.dibujar()
maquina = False
for i in range(9):
    output = []
    if (maquina):
        print('toque cualquier tecla para que juege la maquina')
        input()
        network = resetNet()
        output  = playMove(matchBoard,network)
        pos     = getPosFromOutput(matchBoard,output)
        match.PonerFicha(pos)
    else:
        print('ingrese la posicion')
        print('[0][1][2]')
        print('[3][4][5]')
        print('[6][7][8]')
        pos = -1
        while not (-1 < pos < 9):
            pos = int(input())
            if not (-1 < pos < 9):
                print('el valor ingresado no está dentro del rango especificado')
    match.PonerFicha(pos)
    matchBoard[ map.index(pos) ] = 1
    matchBoard = getInvertedBoard(matchBoard)
    match.dibujar()
    maquina = not maquina
    if match.getHayGanador():
        break
