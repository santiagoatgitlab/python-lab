from random import randint;
from neurona import Neurona;
from conexion import Conexion;

def funcion_relu(x):
	if x < 0:
		y = 0
	else:
		y = 0 + (x / 2)
	return y

def funcion_nada(x):
	return x

definicion = [
	{ 
		"funcion"		: funcion_nada, 
		"sesgo"			: 1,
		"conexiones" 	: [ 
			{
				"peso" 				: 0.5,
				"indice_destino" 	: 2
			},
			{
				"peso" 				: 0.3,
				"indice_destino" 	: 3
			},
			{
				"peso" 				: 0.2,
				"indice_destino" 	: 4
			}
		]
	},
	{ 
		"funcion"		: funcion_nada, 
		"sesgo"			: 2,
		"conexiones" 	: [ 
			{
				"peso" 				: 1.3,
				"indice_destino" 	: 2
			},
			{
				"peso" 				: 0.3,
				"indice_destino" 	: 3
			},
			{
				"peso" 				: 1.1,
				"indice_destino" 	: 4
			}
		]
	},
	{ 
		"funcion"		: funcion_nada, 
		"sesgo"			: 2,
		"conexiones" 	: [ 
			{
				"peso" 				: -1,
				"indice_destino" 	: 5
			}
		]
	},
	{ 
		"funcion"		: funcion_nada, 
		"sesgo"			: 4,
		"conexiones" 	: [ 
			{
				"peso" 				: 1,
				"indice_destino" 	: 5
			}
		]
	},
	{ 
		"funcion"		: funcion_nada, 
		"sesgo"			: -5,
		"conexiones" 	: [ 
			{
				"peso" 				: 0.8,
				"indice_destino" 	: 5
			}
		]
	},
	{ 
		"funcion"		: funcion_nada, 
		"sesgo"			: 8,
		"conexiones" 	: []
	},
]
	
red = []

for neurona in definicion:
	red.append(Neurona(neurona['funcion'], neurona['sesgo']))

for indice in range(len(definicion)):
	conexiones = []
	for conexion in definicion[indice]['conexiones']:
		conexiones.append(Conexion(conexion['peso'], red[conexion['indice_destino']]))
	red[indice].set_conexiones(conexiones)

for neurona in red:
	neurona.activar()
