from random import randint;
from neurona import Neurona;
from conexion import Conexion;

def funcion_relu(x):
	if x < 0:
		y = 0
	else:
		y = 0 + (x / 2)
	return y

def funcion_nada(x):
	return x

definicion = [
	{ 
		"funcion"		: funcion_relu, 
		"conexiones" 	: [ 1, 2, 3 ]
	},
	{ 
		"funcion"		: funcion_relu, 
		"conexiones" 	: [ 4, 5, 6, 7 ]
	},
	{
		"funcion" 		: funcion_relu,
		"conexiones" 	: [ 4, 5, 6, 7 ],
	},
	{
		"funcion" 		: funcion_relu,
		"conexiones" 	: [ 4, 5, 6, 7 ],
	},
	{
		"funcion" 		: funcion_relu,
		"conexiones" 	: [ 8, 9, 10 ],
	},
	{
		"funcion" 		: funcion_relu,
		"conexiones" 	: [ 8, 9, 10 ],
	},
	{
		"funcion" 		: funcion_relu,
		"conexiones" 	: [ 8, 9, 10 ],
	},
	{
		"funcion" 		: funcion_relu,
		"conexiones" 	: [ 8, 9, 10 ],
	},
	{
		"funcion" 		: funcion_relu,
		"conexiones" 	: [ 11 ],
	},
	{
		"funcion" 		: funcion_relu,
		"conexiones" 	: [ 11 ],
	},
	{
		"funcion" 		: funcion_relu,
		"conexiones" 	: [ 11 ],
	},
	{
		"funcion" 		: funcion_nada,
		"conexiones" 	: [],
	}
]
	
red = []

for neurona in definicion:
	print(neurona)
	red.append(Neurona(neurona['funcion'], randint(0,9)))

for indice in range(len(definicion)):
	print('indice', indice)
	print()
	conexiones = []
	for conexion in definicion[indice]['conexiones']:
		conexiones.append(Conexion(randint(0,9) / 10, red[conexion]))
	red[indice].set_conexiones(conexiones)

for neurona in red:
	neurona.activar()
