import json

network_name = 'hardcodeNNDumb'

# number of neurons for every layer, including input and ouput layers
layers_sizes = [9,9,9]

bias 		= []
functions   = []
connections = []


#
# --- PART 1: NEURONS BIAS ------------------------------------------------------------------------------------------------------
#

# initialize all bias in 0
for hidden_layer_index in range(len(layers_sizes)-2): # bias list will only contain the hidden layers, not input or output
	bias.append([])
	for neuron in range(layers_sizes[hidden_layer_index+1]):
		bias[hidden_layer_index].append(0)

# define specific bias for neurons, remember that layer index 0 in bias list corresponds to
# layer index 1 in connections list because  bias have values only in hidden layers
#
# l: layer index
# n: neuron index
#    l  n
bias[0][0] = 0.9
bias[0][1] = 0.5
bias[0][2] = 0.5
bias[0][3] = 0.5
bias[0][4] = 0.5
	


#
# --- PART 2: NEURONS ACTIVATION FUNCTIONS --------------------------------------------------------------------------------------
#

# assign identity function to all hidden layers
for layer in range(len(layers_sizes)-2):
	functions.append("basicFunction")

#
# --- PART 3: CONNECTION WEIGHTS ------------------------------------------------------------------------------------------------
#

# populate connections list (3 dimensional) with all connection weights initialized in 0
for source_layer in range(len(layers_sizes)-1):
	connections.append([])
	for source_neuron in range(layers_sizes[source_layer]):
		connections[source_layer].append([]);
		for target_neuron in range(layers_sizes[source_layer+1]):
			connections[source_layer][source_neuron].append(0)

# define specific weight for connections -- weights that must remain 0 don't need to be specified
# l: source layer index
# s: source neuron index
# t: target neuron index -- target layer index is not needed since it is assumed to be source layer index + 1
# w: connection weight

#           l  s  t	   w	
connections[1][0][0] = 1
connections[1][1][1] = 1
connections[1][2][2] = 1
connections[1][3][3] = 1
connections[1][4][4] = 1
connections[1][5][5] = 1
connections[1][6][6] = 1
connections[1][7][7] = 1
connections[1][8][8] = 1

network_values = {
    "bias"          : bias,
    "functions"     : functions,
    "connections"   : connections,
    "layers_sizes"  : layers_sizes
}

json_values = json.dumps(network_values)
text_file = open(network_name + '.json', 'w')
text_file.write(json_values)
text_file.close()
