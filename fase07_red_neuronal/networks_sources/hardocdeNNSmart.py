import json

network_name = 'tateti'

# number of neurons for every layer, including input and ouput layers
layers_sizes = [9,9,24,9,9]

bias 		= []
functions   = []
connections = []


#
# --- PART 1: NEURONS BIAS ------------------------------------------------------------------------------------------------------
#

# initialize all bias in 0
for hidden_layer_index in range(len(layers_sizes)-2): # bias list will only contain the hidden layers, not input or output
	bias.append([])
	for neuron in range(layers_sizes[hidden_layer_index+1]):
		bias[hidden_layer_index].append(0)

# define specific bias for neurons, remember that layer index 0 in bias list corresponds to
# layer index 1 in connections list because  bias have values only in hidden layers
#
# l: layer index
# n: neuron index
#    l  n
bias[2][0] = 0.9
bias[2][1] = 0.5
bias[2][2] = 0.5
bias[2][3] = 0.5
bias[2][4] = 0.5
	


#
# --- PART 2: NEURONS ACTIVATION FUNCTIONS --------------------------------------------------------------------------------------
#

# assign identity function to all hidden layers
for layer in range(len(layers_sizes)-2):
	functions.append("basicFunction")

# assign specific functions to layers
functions[0] = "reduceNegatives"
functions[1] = "power3abs"



#
# --- PART 3: CONNECTION WEIGHTS ------------------------------------------------------------------------------------------------
#

# populate connections list (3 dimensional) with all connection weights initialized in 0
for source_layer in range(len(layers_sizes)-1):
	connections.append([])
	for source_neuron in range(layers_sizes[source_layer]):
		connections[source_layer].append([]);
		for target_neuron in range(layers_sizes[source_layer+1]):
			connections[source_layer][source_neuron].append(0)

# define specific weight for connections -- weights that must remain 0 don't need to be specified
# l: source layer index
# s: source neuron index
# t: target neuron index -- target layer index is not needed since it is assumed to be source layer index + 1
# w: connection weight

#           l  s  t	   w	
connections[0][0][0] = 1
connections[0][1][1] = 1
connections[0][2][2] = 1
connections[0][3][3] = 1
connections[0][4][4] = 1
connections[0][5][5] = 1
connections[0][6][6] = 1
connections[0][7][7] = 1
connections[0][8][8] = 1

#           l  s  t	    w	
connections[1][0][0]  = 1
connections[1][0][19] = 1
connections[1][0][21] = 1
connections[1][0][22] = 1
connections[1][0][17] = 1
connections[1][0][14] = 1
connections[1][0][11] = 1
connections[1][0][5]  = 1
connections[1][0][8]  = 1
connections[1][1][18] = 1
connections[1][1][16] = 1
connections[1][1][14] = 1
connections[1][1][10] = 1
connections[1][1][7]  = 1
connections[1][1][1]  = 1
connections[1][2][16] = 1
connections[1][2][15] = 1
connections[1][2][11] = 1
connections[1][2][4]  = 1
connections[1][2][3]  = 1
connections[1][2][20] = 1
connections[1][3][23] = 1
connections[1][3][18] = 1
connections[1][3][13] = 1
connections[1][3][8]  = 1
connections[1][3][6]  = 1
connections[1][3][3]  = 1
connections[1][4][23] = 1
connections[1][4][20] = 1
connections[1][4][12] = 1
connections[1][4][9]  = 1
connections[1][4][5]  = 1
connections[1][4][1]  = 1
connections[1][5][7]  = 1
connections[1][5][22] = 1
connections[1][5][4]  = 1
connections[1][5][0]  = 1
connections[1][6][2]  = 1
connections[1][6][6]  = 1
connections[1][6][10] = 1
connections[1][6][21] = 1
connections[1][7][2]  = 1
connections[1][7][9]  = 1
connections[1][7][19] = 1
connections[1][7][15] = 1
connections[1][8][0]  = 1
connections[1][8][12] = 1
connections[1][8][13] = 1
connections[1][8][17] = 1

#           l  s  t	    w	
connections[2][0][0]  = 1
connections[2][0][0]  = 1
connections[2][1][0]  = 1
connections[2][2][0]  = 1
connections[2][3][0]  = 1
connections[2][4][1]  = 1
connections[2][5][1]  = 1
connections[2][6][1]  = 1
connections[2][7][2]  = 1
connections[2][8][2]  = 1
connections[2][9][2]  = 1
connections[2][10][3] = 1
connections[2][11][3] = 1
connections[2][12][3] = 1
connections[2][13][4] = 1
connections[2][14][4] = 1
connections[2][15][4] = 1
connections[2][16][5] = 1
connections[2][17][5] = 1
connections[2][18][6] = 1
connections[2][19][6] = 1
connections[2][20][7] = 1
connections[2][21][7] = 1
connections[2][22][8] = 1
connections[2][23][8] = 1

#           l  s  t	   w	
connections[3][0][0] = 1
connections[3][1][1] = 1
connections[3][2][2] = 1
connections[3][3][3] = 1
connections[3][4][4] = 1
connections[3][5][5] = 1
connections[3][6][6] = 1
connections[3][7][7] = 1
connections[3][8][8] = 1

network_values = {
    "bias"          : bias,
    "functions"     : functions,
    "connections"   : connections,
    "layers_sizes"  : layers_sizes
}

json_values = json.dumps(network_values)
text_file = open(network_name + '.json', 'w')
text_file.write(json_values)
text_file.close()
