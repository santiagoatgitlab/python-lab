from flask import Flask, render_template, url_for
app = Flask(__name__)

@app.route("/")
def hello():
	urls = {
		"dinos_url" : url_for('dinos'),
		"paises_url" : url_for('paises')
	}
	return render_template('main.html', **urls)

@app.route("/dinos")
@app.route("/dinos/<int:lines>")
def dinos(lines=1):
	return render_template('dinos.html', title='Dinosaurios')

@app.route("/paises")
def paises():
	return render_template('paises.html', title='Paises')
