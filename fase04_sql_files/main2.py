from os import getenv
import pymssql

fileHandler = open('Archivo para leer.txt', 'r')
archivo     = fileHandler.readlines()
fileHandler.close()

conexion    = pymssql.connect(server='monostematicos.com', user='hernan', password='mage', database='beta-ori_sistema')
cursor      = conexion.cursor()

campos      = {'dni':'', 'nombre':'','apellido':'','direccion':'','distrito':'','sexo':'','programador':'Santiago'}
separadores	= ['=','/',':',' ']

def primer_ocurrencia(cadena, caracteres):
    posicion = -1
    for separador in separadores:
        primer_ocurrencia = linea.find(separador)
        if primer_ocurrencia > -1 and (posicion == -1 or primer_ocurrencia < posicion):
            posicion = primer_ocurrencia
    return posicion

for linea in archivo:
    posicion_separador = primer_ocurrencia(linea,separadores)
    clave 	= linea[:posicion_separador].strip()
    valor 	= linea[posicion_separador+1:].strip()
    print("separador: " + linea[posicion_separador] + " | posicion: " + str(posicion_separador) + " | campo: " + clave + " | valor: " + valor)

    for campo in campos:
        if clave.lower() == campo:
            campos[campo] = valor
            break

query = "EXEC InsertarPersonaEnPadron '{}','{}','{}','{}','{}','{}','{}'".format(campos['dni'],campos['nombre'],campos['apellido'],campos['direccion'],campos['distrito'],campos['sexo'],campos['programador'])

cursor.execute(query)
resultado = cursor.fetchone()
print(resultado)
