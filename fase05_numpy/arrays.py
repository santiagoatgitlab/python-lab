import numpy as np

#
# creating one dimensional arrays
#
print('\none dimensional arrays\n')
a = np.array([1,3,6]) # all elements are of the int64 type
b = np.array([1.2,3.5,8]) # all elements are of the float64 type, even those without comma
print(a)
print(a.dtype)
print(b)
print(b.dtype)
number = b[2]
print(number)


# specifying the type
c = np.array([1,4,5], dtype=np.float64)
print(c)
print(c.dtype)

#
# creating multidimensional arrays
#
print('\nmultidimensional arrays\n')
d = np.array([(1.5,2,3), (4,5,6)])
e = np.array([[(1.5,2,3)], [(4,5,6), (1 , 2, 4)]])
print(d)
print(e)

#
# filling arrays with something
#
print('\nfilling arrays with something\n')
f = np.zeros( (3,4) ) 	# with zeroes!
g = np.ones( (8,3) ) 	# with ones!
h = np.empty( (13,2) ) 	# with random!!!
print(f)
print(f.dtype)
print(g)
print(g.dtype)
print(h)
print(h.dtype)

#
# creating sequences
#
print('\ncreating sequences\n')
i = np.arange( 2, 31, 4)    # third argument is the step
j = np.arange( 9, 64, 3.2)  # decimal step
print(i)
print(j)

k = np.linspace( 0, 2, 9) # with linspace third argument is the total number of elements instead of the step
print(k)

#
# operations between arrays
#
print('\noperations\n')
l = np.array([20,30,40,50])
m = np.arange(4,8,1)
print(l-m)
print(l+m)
print(l*m)
