from tablero import Tablero
class Peon(Pieza):
	
	tablero = None

	def __init__(self, color, tablero_):
		self.color 		= color
		if tablero == None:
			tablero = tablero_

	def movimientosPosibles(coordenada):
		posibles_movimientos = []
		if coordenada.arriba:
			posibles_movimientos.append(coordenada.arriba)
			if coordenara.arriba.izquierda and tablero.casilla(coordenada.arriba.izquierda):
				posibles_movimientos.append(coordenada.arriba.izquierda)
			if coordenara.arriba.derecha and tablero.casilla(coordenada.arriba.derecha):
				posibles_movimientos.append(coordenada.arriba.derecha)
		if coordenada.y == 2 and self.color == Tablero.colores.BLANCO:
			posibles_movimientos.append(Coord(coordenada.x,4))
		if coordenada.y == 7 and self.color == Tablero.colores.NEGRO:
			posibles_movimientos.append(Coord(coordenada.x,6))
