from tablero import Tablero
class Peon(Pieza):
	
	tablero = None

	def __init__(self, color, tablero_):
		self.color = color
		if tablero == None:
			tablero = tablero_

	def movimientosPosibles(coordenada):
		posibles_movimientos = []
		coordenada_original = coordenada
		while(coordenada.arriba):
			if (coordenada.arriba.izquierda):
				posibles_movimientos.append(coordenada.arriba.izquierda)
				if tablero.casilla(coordenada.arriba.izquierda) != None:
					break
				coordenada = coordenada.arriba.izquierda
			else:
				break
		coordenada = coordenada_original
		while(coordenada.arriba):
			if (coordenada.arriba.derecha):
				posibles_movimientos.append(coordenada.arriba.derecha)
				if tablero.casilla(coordenada.arriba.derecha) != None:
					break
				coordenada = coordenada.arriba.derecha
			else:
				break
		coordenada = coordenada_original
		while(coordenada.abajo):
			if (coordenada.abajo.izquierda):
				posibles_movimientos.append(coordenada.abajo.izquierda)
				coordenada = coordenada.abajo.izquierda
				if tablero.casilla(coordenada.abajo.izquierda) != None:
					break
			else:
				break
		coordenada = coordenada_original
		while(coordenada.abajo):
			if (coordenada.abajo.derecha):
				posibles_movimientos.append(coordenada.abajo.derecha)
				coordenada = coordenada.abajo.derecha
				if tablero.casilla(coordenada.abajo.derecha) != None:
					break
			else:
				break
		return posibles_movimientos
