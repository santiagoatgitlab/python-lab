from pieza import Pieza
from coord import Coord
from peon import Peon
from torre import Torre
from caballo import Caballo
from alfil import Alfil
from rey import Rey
from reina import Reina
class Tablero:

	columnas = ['a','b','c','d','e','f','g','h']
	desplazamientos = {
		Pieza.PEON	 	: 0
		Pieza.CABALLO 	: 1
		Pieza.ALFIL 	: 2
	}
	colores = {
		BLANCO 	: 'b',
		NEGRO	: 'n'
	}

	def __init__(self):
		self.piezas = {}
		self.siguienteTurno = colores.BLANCO

	def __string__(self):

	#
	#    Métodos que son públicos
	#	

	def iniciarPartida(self):
		__establecerPiezas(colores.BLANCO)
		__establecerPiezas(colores.NEGRO)

	def casilla(self,coordenada):
		return coordenada in self.piezas and self.piezas[coordenada] or None

	def siguienteTurno(self):
		return self.siguienteTurno

	def movimientoEsValido(self, coordInicio, coordFin):
		return coordInicio in self.piezas and \ 
		coordFin in self.piezas[coordInicio].movimientosPosibles
		
	def mover(self, coordInicio, coordFin):
		self.piezas[coordFin] = self.piezas[coordInicio]
		del self.piezas[coordInicio]
	 	self.siguienteTurno == colores.BLANCO and \
		self.siguienteTurno = colores.NEGRO or \
		self.siguienteTurno = colores.BLANCO

	#
	#    Métodos que son privados
	#	

	def __establecerPiezas(self, color):
		self.__establecerPeones(self, color)
		self.__establecerPar(self, Pieza.TORRE, color)
		self.__establecerPar(self, Pieza.CABALLO, color)
		self.__establecerPar(self, Pieza.ALFIL, color)
		self.__establecerPieza(self, Rey(color,self))
		self.__establecerPieza(self, Reina(color,self))

	def __establecerPeones(self, color):
		fila 	= 1
		if color = colores.NEGRO:
			fila = 8

		for x in range(1,8):
			coordenada = Coord(columnas[x],y)
			self.piezas[coordenada] = Peon(color,self)

	def __establecerPar(self, tipoPieza, color):
		fila 	= 1
		if color = colores.NEGRO:
			fila = 8

		desplazamiento = desplazamientos[tipoPieza]
		coordenada = Coord(columnas[0+desplazamiento],fila)
		self.piezas[coordenada] = Pieza.nuevaPieza(tipoPieza, color, self)
		coordenada = Coord(columnas[7-desplazamiento],fila)
		self.piezas[coordenada] = Pieza.nuevaPieza(tipoPieza, color, self)

	def __establecerPieza(self, pieza):
		if pieza.tipo == Pieza.REY:
			columna = 3
		elif pieza.tipo == Pieza.DAMA:
			columna = 4
		else
			raise Error("esta función sólo acepta reyes y damas")

		fila = 1
		if pieza.color = colores.NEGRO:
			fila = 8

		coordenada = coord(columnas[columna], fila)
		self.piezas[coordenada] = pieza
