from peon import Peon
from torre import Torre
from caballo import Caballo
from alfil import Alfil
from rey import Rey
from dama import Dama

class Pieza:

	PEON 	= 0
	TORRE 	= 1
	CABALLO	= 2
	ALFIL 	= 3
	REY 	= 4
	DAMA 	= 5

	def __init__(self):
		raise NotImplementedError
	
	def movimientosPosibles(coordenada):
		raise NotImplementedError

	@staticmethod
	def nuevaPieza(tipoPieza, color, tablero):
		if (tipoPieza == PEON)
			return Peon(color,tablero)
		if (tipoPieza == TORRE)
			return Torre(color,tablero)
		if (tipoPieza == CABALLO)
			return Caballo(color,tablero)
		if (tipoPieza == ALFIL)
			return Alfil(color,tablero)
		if (tipoPieza == REY)
			return Rey(color,tablero)
		if (tipoPieza == DAMA)
			return Dama(color,tablero)

	def casillasArriba(
