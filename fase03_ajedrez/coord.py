class Coord:
	
	coordenadasX 	= ['a','b','c','d','e','f','g','h']

	def __init__(self,x,y):
		self.x = x
		self.y = y

	def __str__(self):
		return self.coordenadasX[self.x] + ',' + str(self.y)

	def arriba(self):
		return (self.y < 8 or None) and Coord(self.coordenadasX[self.x],self.y+1)

	def abajo(self):
		return (self.y > 1 or None) and Coord(self.coordenadasX[self.x],self.y-1)

	def derecha(self):
		return (self.x < 7 or None) and Coord(self.coordenadasX[self.x+1],self.y)

	def izquierda(self):
		return (self.x > 0 or None) and Coord(self.coordenadasX[self.x-1],self.y)

	def __getX(self):
		return self.__x

	def __setX(self,x):
		if x in self.coordenadasX:
			self.__x = self.coordenadasX.index(x)
		else:
			raise Exception("Error al inizializar la coordenada x: se esperaba una letra entre a y h")

	def __getY(self):
		return self.__y

	def __setY(self,y):
		if isinstance(y, int) and y >= 1 and y <= 8:
			self.__y = y
		else:
			raise Exception("Error al inizializar la coordenada y: se esperaba un valor entero entre 1 y 8")	

	x = property(__getX,__setX)
	y = property(__getY,__setY)	

try:
	c = Coord('d',4)
	print(c.arriba())
	print(c.abajo())
	print(c.izquierda())
	print(c.derecha())
except Exception as exception:
	print(exception)
