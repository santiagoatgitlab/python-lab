from tablero import Tablero
class Peon(Pieza):
	
	tablero = None

	def __init__(self, color, tablero_):
		self.color 		= color
		if tablero == None:
			tablero = tablero_

	def movimientosPosibles(coordenada):
		posibles_movimientos = []
		while (coordenada.arriba()):
			posibles_movimientos.append(coordenada.arriba)
			if (tablero.casilla(coordenada.arriba) != None):
				break
		while (coordenada.abajo()):
			posibles_movimientos.append(coordenada.abajo)
			if (tablero.casilla(coordenada.abajo) != None):
				break
		while (coordenada.izquierda()):
			posibles_movimientos.append(coordenada.izquierda)
			if (tablero.casilla(coordenada.izquierda) != None):
				break
		while (coordenada.derecha()):
			posibles_movimientos.append(coordenada.derecha)
			if (tablero.casilla(coordenada.derecha) != None):
				break
		return posibles_movimientos
