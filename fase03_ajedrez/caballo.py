from tablero import Tablero
class Peon(Pieza):
	
	tablero = None

	def __init__(self, color, tablero_):
		self.color 		= color
		if tablero == None:
			tablero = tablero_

	def movimientosPosibles(coordenada):
		posibles_movimientos = []
		if coordenada.arriba:
			if coordenada.arriba.arriba:
				if coordenada.arriba.izquierda:
					posibles_movimientos.append(coordenada.arriba.izquierda)
				if coordenada.arriba.derecha:
					posibles_movimientos.append(coordenada.arriba.derecha)
		if coordenada.abajo:
			if coordenada.abajo.abajo:
				if coordenada.abajo.izquierda:
					posibles_movimientos.append(coordenada.abajo.izquierda)
				if coordenada.abajo.derecha:
					posibles_movimientos.append(coordenada.abajo.derecha)
		if coordenada.izquierda:
			if coordenada.izquierda.izquierda:
				if coordenada.izquierda.arriba:
					posibles_movimientos.append(coordenada.izquierda.arriba)
				if coordenada.izquierda.abajo:
					posibles_movimientos.append(coordenada.izquierda.abajo)
		if coordenada.derecha:
			if coordenada.derecha.derecha:
				if coordenada.derecha.arriba:
					posibles_movimientos.append(coordenada.derecha.arriba)
				if coordenada.derecha.abajo:
					posibles_movimientos.append(coordenada.derecha.abajo)
		return posibles_movimientos
