import random

difference 	= 1890
dicesNumber = 5
 
games = {
	"nada" 		: 1,
	"par"		: 15,
	"doblepar"	: 35,
	"trio"		: 385,
	"full"		: 13,
	"poker"		: 478776,
	"escalera"	: 191511,
	"generala"	: 3830221
}

def showDices(dices):
	print('your dices:')
	for x in range(1,len(dices)+1):
		print('dado ' + str(x) + ': ' + str(dices[x-1]))

def checkResult(dices):
	suma	= 0
	for d in dices:
		suma += d
	points = 0
	results = [0,0,0,0,0,0]	
	for val in dices:
		results[val-1] += 1

	print(str(dices))

	if 5 in results:
		points = games["generala"] * (results.index(5)+1)
	else:
		noEscalera = False
		if (results.count(0) > 1):
			noEscalera = True
		if not noEscalera:
			if (results.index(0) != 0 and results.index(0) != 5):
				noEscalera = True	
		if not noEscalera:
			points = games["escalera"] * suma + difference
			print("escalera")
		else:
			if 4 in results:
				points = ( games["poker"] * (results.index(4)+1) ) + (results.index(1)+1) + difference
				print("poker")

			else:
				if 3 in results:
					if 2 in results:
						tripleFactor 	= ( (games["full"]*(results.index(3)+1)) ** 3)
						doubleFactor 	= ( (games["full"]*(results.index(2)+1)) ** 2)
						points 			= tripleFactor + doubleFactor + difference
						print("full")
					else:
						restante1					= results.index(1)+1
						results[ results.index(1) ] = 0
						restante2					= results.index(1)+1
						points						= games["trio"] * (results.index(3)+1) + restante1 + restante2 + difference
						print("trio")
				else:
					if 2 in results:
						if results.count(2) == 2:
							par1						= results.index(2)+1
							results[ results.index(2) ] = 0
							par2						= results.index(2)+1
							resto						= results.index(1)+1
							point = 0
							if par1 > par2:
								grande = par1
								chico = par1
							else:
								grande = par2
								chico = par1
							points = (grande * games["doblepar"] * 10) + chico * games["doblepar"] + resto

								
							print("par doble")
						else:
							restante1					= results.index(1)+1
							results[ results.index(1) ] = 0
							restante2					= results.index(1)+1
							results[ results.index(1) ] = 0
							restante3					= results.index(1)+1
							points						= games["par"] * (results.index(2)+1) + restante1 + restante2 + restante3
							print("par")
					else:
						points = suma
	return points

dices = {
	"player1" : [],
	"player2" : []
}

sums = {
	"player1" : 0,
	"player2" : 0
}

for keyCurrentPlayer, dicesCurrentPlayer in dices.items():
	print("\n", keyCurrentPlayer, "\n")
	for i in range(0,dicesNumber):
		dices[keyCurrentPlayer].append(random.randint(1,6))

	showDices(dicesCurrentPlayer)

	for i in range(0,2):

		entry 			= None
		dicesToChange 	= [] 

		while (entry != 0 and len(dicesToChange) < dicesNumber):
			print ('enter a number of dice to change or 0 to finish')
			entry = int(input())
			if (0 < entry < 6):
				dicesToChange.append(entry)

		for x in dicesToChange:
			dices[keyCurrentPlayer][x-1] = random.randint(1,6)
		showDices(dicesCurrentPlayer)

for keyCurrentPlayer, dicesCurrentPlayer in dices.items():
	for x in dicesCurrentPlayer:
		sums[keyCurrentPlayer] += x;
	print (keyCurrentPlayer + " points: " + str(sums[keyCurrentPlayer]))

def ResultadoPartido(dices1,dices2):
	result1 = checkResult(dices1)
	result2 = checkResult(dices2)
	print("resultado 1: ",result1)
	print("resultado 2: ",result2)
	if result1 > result2:
		return 1
	elif result2 > result1:
		return 2
	else:
		return 3

res =  ResultadoPartido(dices['player1'],dices['player2'])
if res == 3:
	print('juego empatado')
else:
	print('player ' + str(res) + ' wins')
