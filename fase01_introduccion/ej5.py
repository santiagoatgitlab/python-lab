import random

dicesNumber = 5

def showDices(dices):
	print('your dices:')
	for x in range(1,len(dices)+1):
		print('dado ' + str(x) + ': ' + str(dices[x-1]))

dices = {
	"player1" : [],
	"player2" : []
}

sums = {
	"player1" : 0,
	"player2" : 0
}

for keyCurrentPlayer, dicesCurrentPlayer in dices.items():
	print("\n", keyCurrentPlayer, "\n")
	for i in range(0,dicesNumber):
		dices[keyCurrentPlayer].append(random.randint(1,6))

	showDices(dicesCurrentPlayer)

	for i in range(0,2):

		entry 			= None
		dicesToChange 	= [] 

		while (entry != 0 and len(dicesToChange) < dicesNumber):
			print ('enter a number of dice to change or 0 to finish')
			entry = int(input())
			if (0 < entry < 6):
				dicesToChange.append(entry)

		for x in dicesToChange:
			dices[keyCurrentPlayer][x-1] = random.randint(1,6)
		showDices(dicesCurrentPlayer)

for keyCurrentPlayer, dicesCurrentPlayer in dices.items():
	for x in dicesCurrentPlayer:
		sums[keyCurrentPlayer] += x;
	print (keyCurrentPlayer + " points: " + str(sums[keyCurrentPlayer]))

if (sums["player1"] > sums["player2"]):
	print("player 1 wins!")
elif (sums["player2"] > sums["player1"]):
	print("player 2 wins!")
else:
	print("draw game")
