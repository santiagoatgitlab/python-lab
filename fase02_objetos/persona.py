import random
import time

class Persona:

	def __init__ (self, padre=None, madre=None): 
		self.padre 			= padre
		self.madre 			= madre
		self.birthTime 		= None
		self.deathTime		= None
		self.edad 			= 0
		self.memories 		= []
		self.name			= ""
		self.evocationSkill	= 1

	def __str__(self):
		result = self.name + ', ' + str(self.edad) + ' año'
		result += 's' if self.edad != 1 else ''
		return result

	def bautizar(self, name):
		if self.name == "":
			self.saveMemory("Me bautizaron")
			self.name = name
			return True
		else:
			self.saveMemory("Me intentaron bautizar de nuevo")
		return False

	def cumplirAnios(self):
		self.edad += 1
		memoryDescription = "Cumplí " + str(self.edad) + " año"
		memoryDescription += 's' if self.edad != 1 else ''
		self.saveMemory(memoryDescription)

	def esHuerfano(self):
		return self.padre == None and self.madre == None

	def obtenerRecuerdos(self):
		finalMemories		= []
		numberOfMemories 	= int(len(self.memories) * self.evocationSkill)
		rememberedMemories	= []
		while True:
			currentMemoryNumber = random.randint(0,len(self.memories)-1)
			if not currentMemoryNumber in rememberedMemories:
				rememberedMemories.append(currentMemoryNumber)
			if len(rememberedMemories) == numberOfMemories:
				break;
		for i in rememberedMemories:
			finalMemories.append(self.memories[i]["description"])
		return finalMemories

	def esHermanoDe(self, posibleHermano):
		if self.padre != None and self.padre == posibleHermano.padre:
			return True
		return self.madre != None and self.madre == posibleHermano.madre

	def morir(self):
		deathTime = time.time()

	def estaVivo(self):
		return self.deathTime is None

	def saveMemory(self, memoryDescription):
		memory = {
			"description" 	: memoryDescription,
			"time"			: time.time()
		}
		self.memories.append(memory)
