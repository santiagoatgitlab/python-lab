api_url = 'http://localhost:5000/api/move';
console.log('tateti js loaded')
pieces_map = {
    ""  : 0,
    "cross" : 1,
    "circle" : -1
};
board           = [ 0,0,0,0,0,0,0,0,0 ];
board_map 		= [ 1,5,2,6,0,7,3,8,4 ]
current_piece   = "cross";
parity          = 1;

thinking        = false;

function init(){
    cells = document.querySelectorAll('.tateti-cell');
    for (cell of cells){
        cell.addEventListener('click', onCellClicked, true);
    }
    playButtons = document.querySelectorAll('.player .play');
    for (button of playButtons){
        button.addEventListener('click', onPlayButtonClicked, true);
    }
}

function select_cell(number){
    if (board[number] == 0){
        set_piece(number);
    }
}

function set_piece(number){

    board[number] = pieces_map[current_piece]
    let cell = document.querySelector('.cell_' + number);
    cell.classList.remove('free');
    cell.classList.add('full');
    cell.classList.add(current_piece);
    switch_turn();
    checkForWinner();

}

function switch_turn(){
    if (current_piece == "cross"){
        current_piece = "circle";
    }
    else{
        current_piece = "cross";
    }
    parity = parity * -1;
}

function get_logic_board(board){
    logic_board = [0,0,0,0,0,0,0,0,0];
    for (i=0; i<board.length; i++){
        logic_board[board_map[i]] = board[i];
    }
    return logic_board;
}

function getBoardWithParity(board,parity){
    parityBoard = [];
    for (i=0; i<board.length; i++){
        parityBoard[i] = board[i] * parity;
    }
    return parityBoard;
}

function remoteMove(button){ // ev is for event!

    thinking            = true;
    let action          = button.closest('.action');
    let network_name    = button.dataset.name;

    action.querySelector('.thinking').classList.add('show');
    let xhr = new XMLHttpRequest();
    
    let parity_board    = getBoardWithParity(board,parity);
    logic_board         = get_logic_board(parity_board);
	board_string = JSON.stringify(logic_board);
	current_api_url = api_url + '?board=' + board_string + '&network=' + network_name;
	xhr.open('GET',current_api_url);
	xhr.onload = function () {

		// Process our return data
		if (xhr.status >= 200 && xhr.status < 300) {
			// This will run when the request is successful
            set_piece(parseInt(xhr.response));
		} else {
			// This will run when it's not
			console.log('The request failed!');
		}

        thinking = false;
        action.querySelector('.thinking').classList.remove('show');

	};
	xhr.send();
}

function checkForWinner(){
    let winningPositions = [
        [ 0, 3, 6 ], // vertical 1
        [ 1, 4, 7 ], // vertical 2
        [ 2, 5, 8 ], // vertical 3
        [ 0, 1, 2 ], // horizontal 1
        [ 3, 4, 5 ], // horizontal 2
        [ 6, 7, 8 ], // horizontal 3
        [ 0, 4, 8 ], // diagonal 1
        [ 2, 4, 6 ], // diagonal 2  
    ];
    for (i = 0; i < winningPositions.length; i++){
        if (    board[winningPositions[i][0]] != 0 
             && board[winningPositions[i][0]] == board[winningPositions[i][1]]
             && board[winningPositions[i][0]] == board[winningPositions[i][2]]
             ){
            markWinner(winningPositions[i]);
            break;
        }
    }
}

function markWinner(cells){
    console.log('mark winner', cells);
    for (number of cells){
        let cell = document.querySelector('.cell_' + number);
        cell.classList.add('winning-line');
    }
}

init();

/* --------- Event handlers --------- */

function onCellClicked(ev){
    if (!thinking){
        number = ev.target.dataset.number;
        select_cell(number);
    }
}

function onPlayButtonClicked(ev){
    if (!thinking){
        remoteMove(ev.target);
    }
}
