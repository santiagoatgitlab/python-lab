from flask import Flask, render_template, url_for, request
import os
import sys
import json
import logging
import mover

app = Flask(__name__)
app.config.from_object('config')

logging.basicConfig(filename='log/debug.log',level=logging.DEBUG,format='[%(levelname)s] %(asctime)s %(message)s', datefmt='%Y/%m/%dT%H:%M:%S')

@app.route("/")
def home():
    return render_template('welcome.html')

@app.route("/old_example")
def hello():
	urls = {
		"dinos_url" : url_for('dinos'),
		"paises_url" : url_for('paises')
	}
	return render_template('main.html', **urls)

@app.route("/dinos")
@app.route("/dinos/<int:lines>")
def dinos(lines=1):
	return render_template('dinos.html', title='Dinosaurios')

@app.route("/paises")
def paises():
	return render_template('paises.html', title='Paises')

@app.route("/tateti")
def tateti():
	return render_template('tateti.html')

@app.route("/api/move")
def move():
    boardString     = request.args.get('board','[0,0,0,0,0,0,0,0,0]')
    board           = json.loads(boardString)
    network_name    = request.args.get('network','default')

    network_file    = open(os.path.abspath(app.config['APP_PATH']+app.config['NETWORKS_PATH']+network_name+".json"),"r")
    network_json    = network_file.read()
    network_values  = json.loads(network_json)
    move            = mover.calculatePos(board,network_values)
    return str(move)
