import logging
class Neuron:
    def __init__(self, activation_function, bias, network, index):
        self.index                  = index
        self.network                = network
        self.result                 = 0
        self.activation_function	= activation_function
        self.bias  				    = bias
        self.input_value            = bias
        self.connections            = []
    def setBias(self, bias):
        self.bias           = bias
        self.input_value    = bias
    def setConnections(self, connections):
        self.connections = connections
    def activate(self):
        self.result = self.activation_function(self.input_value)
        for connection in self.connections:
            #  if self.index == 0:
            self.network[connection.target_neuron_index].input_value += self.result * connection.weight
        return self.result
