from neuron import Neuron
from connection import Connection
import logging
import math

logging.basicConfig(filename='log/debug.log',level=logging.DEBUG,format='[%(levelname)s] %(asctime)s %(message)s', datefmt='%Y/%m/%dT%H:%M:%S')

def basicFunction(x):
    return x

def reduceNegatives(x):
    if (x < 0):
        return x * 0.95 
    else:
        return x

def power3abs(x):
    result = math.fabs(x**4)
    return result

def calculatePos(inputs,network_values):

    functions = {
        "basicFunction"     : basicFunction,
        "reduceNegatives"   : reduceNegatives,
        "power3abs"         : power3abs
    }

    network = []
    layers_sizes = network_values['layers_sizes']

    for layer_index in range(len(layers_sizes)):
        # output_text += 'layers ' + str(layer_index) + ' length: ' + str(layers_sizes[layer_index]) + '<br>'
        for neuron_index in range(layers_sizes[layer_index]):
            #neuron connections
            neuron_connections      = []
            if layer_index < len(layers_sizes)-1:
                target_neurons_offset   = 0
                previous_layer_index    = 0
                while previous_layer_index <= layer_index:
                    target_neurons_offset += layers_sizes[previous_layer_index]
                    previous_layer_index += 1
                for target_neuron_index in range(layers_sizes[layer_index+1]):
                    weight = network_values["connections"][layer_index][neuron_index][target_neuron_index]
                    neuron_connections.append( Connection(weight,target_neuron_index + target_neurons_offset) )

            #neuron itself
            if layer_index == 0 or layer_index == len(layers_sizes) - 1:
                function    = basicFunction
                bias        = 0
            else:
                function    = functions[network_values["functions"][layer_index-1]] # -1 because in functions list contains only hidden layer, so indexes are shifted respect to the list of all layers
                bias        = network_values["bias"][layer_index-1][neuron_index]   # -1 because in bias list contains only hidden layer, so indexes are shifted respect to the list of all layers
            neuron = Neuron(function,bias,network,neuron_index)
            neuron.setConnections(neuron_connections)
            network.append(neuron)

    output      = []
    counter     = 0
    threshold   = len(network)-10
    next_offset = 0
    layer_index = 0
    local_count = 0
    print(inputs)
    for neuron_index in range(layers_sizes[0]):
        network[neuron_index].setBias(inputs[neuron_index])
    for neuron in network:
        res = neuron.activate()
        if counter == next_offset:
            layer_index += 1
            next_offset += layers_sizes[layer_index-1]
            local_count = 0
        if counter > threshold:
            output.append(res)
        counter     += 1
        local_count += 1
    pos = getPosFromOutput(inputs,output)
    return pos

#
# play match
#
def getPosFromOutput(board,outputs):
    map = [4, 0, 2, 6, 8, 1, 3, 5, 7]
    #recorro el list de outputs (orden santi)
    biggestIndex = 0
    biggestValue = 0
    logging.debug('outputs: ' + str(outputs))
    logging.debug('board: ' + str(board))
    for index, value in enumerate(outputs):
        if ( board[index] != 0):
            continue
        if value >= biggestValue:
            biggestValue = value
            biggestIndex = index
    logging.debug('result: ' + str(map[biggestIndex]))
    return map[biggestIndex]
